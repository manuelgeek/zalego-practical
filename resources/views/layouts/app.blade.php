<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('partials.layouts._head')
</head>
<body>
    <div id="app">
        @include('partials.layouts._nav')

        <main class="py-4">

            @include('partials._notify')

            
            @yield('content')
        </main>

        <footer>
            <div class="navbar-fixed-bottom nav navbar ">
                <p class="text-center">{{date('Y') ." ". env("APP_NAME")}}  All Rights Reserved</p>
            </div>
        </footer>
    </div>


     <!-- Small modal -->
                 @auth

                  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">You're about to delete this Account</h4>
                        </div>
                        <div class="modal-body">
                          <h4>Proceed...</h4>
                          
                        </div>
                        <div class="modal-footer">
                          {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> --}}
                          <form method="POST" action="{{ route('profile.destroy',Auth::user()->id) }}">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-danger ">Delete Account</button>
                            </form>
                        </div>

                      </div>
                    </div>
                  </div>

                  @endauth
                  <!-- /modals -->

    <!-- Scripts -->
   <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/parsley.min.js') }}"></script>
</body>
</html>
