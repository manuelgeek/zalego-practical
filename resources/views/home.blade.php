@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h3 class="h3">Welcome <b>{{Auth::user()->username}}</b> to your Account</h3><br>
                    <div class="home-stuff">
                        <h4>Account Info</h4>
                        <br>
                        <p>Name : {{Auth::user()->fname." ".Auth::user()->lname}}</p>
                        <p>Email: {{Auth::user()->email}} </p>
                        <p>Gender: {{Auth::user()->gender}} </p>
                        <p>Language: {{Auth::user()->languages}} </p>
                        <br>

                        <a href="{{ route('profile.edit') }}" class="btn btn-warning">Edit Account</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
