

## Zalego Practical

Webb Project , Practical interview

Runs on Laravel 

## Installation
Composer update

copy .env.example to .env

php artisan key:generate

change DB comfingurations in .env

php artisan migrate

change Email comfigurations in .env

php artisan serve


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
