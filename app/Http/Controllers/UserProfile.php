<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Session;

class UserProfile extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {
    	return view('auth.edit');
    }

    public function update(Request $request)
    {
    	$profile = \App\User::find(Auth::user()->id);

    	$this->validate($request, array(
                'fname' => 'required|string|max:255',
	            'lname' => 'required|string|max:255',
	            'username' => "required|string|max:255|unique:users,username,$profile->id",
	            'gender' => 'required|string|max:25',
	            'languages' => 'required|string|max:20',
	            'email' => "required|string|email|max:255|unique:users,email,$profile->id",
                ));

        $profile->fname = $request->fname;
        $profile->lname = $request->lname;
        $profile->username = $request->username;
        $profile->gender = $request->gender;
        $profile->languages = $request->languages;
        $profile->email = $request->email;

        $profile->save();

        Session::flash('success', ' Profile successfully Updated');

        return redirect()->route('home');
        
    }

    public function destroy($id)
    {
    	$profile = \App\User::findOrFail($id);

    	$profile->delete();

    	Auth::logout();

    	Session::flash('error', ' Profile successfully Deleted');

    	return redirect()->route('home');
    }
}
