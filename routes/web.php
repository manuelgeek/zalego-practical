<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if(Auth::check()){
		return redirect()->route('home');
	}
    return view('auth.register');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/update-profile','UserProfile@edit')->name('profile.edit');
Route::put('/update-profile/edit','UserProfile@update')->name('profile.update');
Route::delete('/update-profile/delete/{id}','UserProfile@destroy')->name('profile.destroy');